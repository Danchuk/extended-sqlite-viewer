#ifndef HEXVIEW_H
#define HEXVIEW_H

#include<QStandardItemModel>
#include<QStandardItem>
#include<QStyledItemDelegate>

class itemHex : public QStandardItem
{

};



class HexView : public QStandardItemModel
{
public:
    HexView();
    void setHexTable( std::vector <std::pair<unsigned int, std::string>> dataHex);
};

#endif // HEXVIEW_H
