#pragma once
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <iterator>
#include <algorithm>
#include "Converter.h"
#include "Gatherer.h"
#include <map>
#include <queue>
#include <tuple>
#include <utility>

class Recoverer
{
public:
    Recoverer();
    void RestoreRecord(std::vector<std::tuple<std::vector<std::string>, std::string, unsigned int, unsigned int, unsigned int, std::vector<unsigned long long>>>&,const unsigned int&);
    void Openfile(const std::string&);
    void SetAllBitmasks(std::vector <std::tuple<std::string, std::vector<std::string>, unsigned int>>&);
	std::map<std::string, std::vector<std::tuple<std::vector<std::string>, std::string, unsigned int, unsigned int, unsigned int, std::vector<unsigned long long>>>> ParseDeletedRecords();
    std::vector<std::string> GatherDeletedRecord(std::vector<unsigned long long>&, std::string&, unsigned int&);
    void ReplaceFile(const std::string&);
    std::vector<std::pair<unsigned int, std::string>> GetFreeBlocks();
    ~Recoverer();
private:
    Gatherer m_gatherer;
    std::string Affinity(const std::string& type);
    void TraverseBTreeRec(std::vector<unsigned int>&, unsigned int);
    void FindDeletedRecord(std::string, std::vector<std::string>&);
    bool ValueCorrespondsMask(unsigned long long&, std::string&);
    long long RestoreInteger(std::string&, unsigned long long&, unsigned int&);
    std::vector<unsigned int> GatherAllLeaves(unsigned int&);
	void RedirectFreeblockNode(unsigned int&, std::pair<unsigned int, std::string>&);
    unsigned int RemoveWholeFreeblock(std::pair<unsigned int, std::string>&);
	unsigned int GetOffsetToFreeblock(std::pair<unsigned int, std::string>&);
    unsigned long long GetRowid(unsigned int&, unsigned int&);
    unsigned long long UpdatePointerArray(unsigned int&,const unsigned int&, const unsigned int&);
    void RestoreRecordHeader(unsigned int&, unsigned long long&, unsigned int&, std::vector<unsigned long long>&);
    void RestoreSimpleRecord(std::pair<unsigned int, std::string>&, const unsigned int&, std::vector<unsigned long long>&);
	void RestorePartOfRecord(std::pair<unsigned int, std::string>&, const unsigned int&, std::vector<unsigned long long>&, std::pair<unsigned int, unsigned int>&);
	void RestoreBeginning(std::pair<unsigned int, std::string>&, const unsigned int&, std::vector<unsigned long long>&, std::pair<unsigned int, unsigned int>&);
	void RestoreMiddle(std::pair<unsigned int, std::string>&, const unsigned int&, std::vector<unsigned long long>&, std::pair<unsigned int, unsigned int>&);
	void RestoreEnding(std::pair<unsigned int, std::string>&, const unsigned int&, std::vector<unsigned long long>&, std::pair<unsigned int, unsigned int>&);
    std::string m_hexChars;
    std::deque<std::tuple<std::vector<std::string>, unsigned int, unsigned int, std::vector<unsigned long long>>> m_foundedValues;
    std::vector <std::pair<unsigned int, std::string>> m_freeblocks;
    std::vector <std::tuple<std::string, std::vector<std::string>, std::vector<unsigned int>>> m_bitmask;
    unsigned int m_pageSize;
    static std::map<std::string, int> m_affinitiesmap;
};

