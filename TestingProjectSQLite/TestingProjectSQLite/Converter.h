#pragma once
#include <string>
#include <sstream>
#include <vector>
#include <bitset>

class Converter
{
public:
    static void RawToHex(const std::vector<unsigned char>&, std::string&);
    static unsigned long long ConvertFromHex(std::string &);
    static unsigned long long ConvertFromHex(std::string &&);
	static std::string ConvertToText(std::string &);
	static std::string ConvertToText(std::string &&);
    static unsigned long long ReadVarint(const std::string&, unsigned int&);
    static std::string WriteVarint(const unsigned long long&);
private:
	static const std::string m_hex;
	static void RawToHex(const unsigned char *data, size_t length, std::string &str);
	static void GatherBits(std::string &bitset, std::vector<unsigned char> chars);
};

