#include "Gatherer.h"


void Gatherer::GatherFreeblocks(std::string &hexText, unsigned int& offset, unsigned int& pagenum, unsigned int& pagesize, std::vector <std::pair<unsigned int, std::string>>& freeblocks)
{
    std::string freeblockinfo;
    unsigned int nextFreeBlock;
    unsigned int blockSize;
    do
    {
        freeblockinfo = std::string(hexText.begin() + 2 * offset, hexText.begin() + 2 * offset + 8);
        nextFreeBlock = Converter::ConvertFromHex(std::string(freeblockinfo.begin(), freeblockinfo.begin() + 4));
        blockSize = Converter::ConvertFromHex(std::string(freeblockinfo.begin() + 4, freeblockinfo.end()));

        std::string freeBlock(hexText.begin() + 2 * offset, hexText.begin() + 2 * offset + 2 * blockSize);
        if (freeBlock != "")
        {
            freeblocks.push_back(std::make_pair(pagenum, freeBlock));
        }
        offset = nextFreeBlock + pagenum * pagesize;
    } while (nextFreeBlock != 0);
}

std::vector <std::pair<unsigned int, std::string>> Gatherer::LoopThroughPages(std::streampos& m_file ,std::string& hexText, unsigned int & m_pageSize)
{
    std::vector<std::pair<unsigned int, std::string>> freeblocks;
    std::string substr(hexText.begin() + 32, hexText.begin() + 36);
    m_pageSize = Converter::ConvertFromHex(substr);
    if (m_pageSize == 1)
    {
        m_pageSize = 65536;
    }
    auto iter = m_pageSize;
    unsigned int pagenum = 1;
    while (iter < m_file)
    {
        std::string header(hexText.begin() + 2 * iter, hexText.begin() + 2 * iter + 16);
        std::string pageType(std::string(header.begin(), header.begin() + 2));
        if (IsValidPage(pageType))
        {
            std::string freeBlockOffset(header.begin() + 2, header.begin() + 6);
            unsigned int offset = iter + Converter::ConvertFromHex(freeBlockOffset);
            if (offset != iter)
            {
                GatherFreeblocks(hexText, offset, pagenum, m_pageSize, freeblocks);
            }
        }
        iter += m_pageSize;
        pagenum++;
    }
    return freeblocks;
}

bool Gatherer::IsValidPage(const std::string& pageType)
{
    return pageType == "0D" || pageType == "05" || pageType == "02" || pageType == "0A";
}
