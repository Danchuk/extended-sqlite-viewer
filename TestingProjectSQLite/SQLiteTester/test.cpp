#include "pch.h"
#include "TestingProjectSQLite/Converter.h"
#include "TestingProjectSQLite/Recoverer.h"

TEST(TestConversionToText, TestSimpleWordConversion) {
	std::string input("576F726C64");
	std::string expected("World");
	std::string actual = Converter::ConvertToText(input);
	EXPECT_STREQ(expected.c_str(), actual.c_str());
}

TEST(TestConversionToNum, SimpleTest) {
	std::string input("1000");
	int expected = 4096;
	int actual = Converter::ConvertFromHex(input);
	EXPECT_EQ(expected, actual);
}

TEST(TestReadVarint, OneByte) {
	std::string input("39");
	unsigned long long expected = 57;
	unsigned int bytesread = 0;
	unsigned long long actual = Converter::ReadVarint(input, bytesread);
	EXPECT_EQ(expected, actual);
}

TEST(TestReadVarint, TwoBytes) {
	std::string input("8239");
	unsigned long long expected = 313;
	unsigned int bytesread = 0;
	unsigned long long actual = Converter::ReadVarint(input, bytesread);
	EXPECT_EQ(expected, actual);
}

TEST(TestReadVarint, ThreeBytes) {
	std::string input("82992F");
	unsigned long long expected = 36015;
	unsigned int bytesread = 0;
	unsigned long long actual = Converter::ReadVarint(input, bytesread);
	EXPECT_EQ(expected, actual);
}

TEST(TestReadVarint, FourBytes) {
	std::string input("8299AF11");
	unsigned long long expected = 4609937;
	unsigned int bytesread = 0;
	unsigned long long actual = Converter::ReadVarint(input, bytesread);
	EXPECT_EQ(expected, actual);
}

TEST(TestReadVarint, FiveBytes) {
	std::string input("E299AFD101");
	unsigned long long expected = 26359883905;
	unsigned int bytesread = 0;
	unsigned long long actual = Converter::ReadVarint(input, bytesread);
	EXPECT_EQ(expected, actual);
}

TEST(TestReadVarint, SixBytes) {
	std::string input("E299AFD1FF13");
	unsigned long long expected = 3374065155987;
	unsigned int bytesread = 0;
	unsigned long long actual = Converter::ReadVarint(input, bytesread);
	EXPECT_EQ(expected, actual);
}

TEST(TestReadVarint, SevenBytes) {
	std::string input("E299AFD1FF931A");
	unsigned long long expected = 431880339966362;
	unsigned int bytesread = 0;
	unsigned long long actual = Converter::ReadVarint(input, bytesread);
	EXPECT_EQ(expected, actual);
}

TEST(TestReadVarint, EightBytes) {
	std::string input("E299AFD1FF93BA02");
	unsigned long long expected = 55280683515698434;
	unsigned int bytesread = 0;
	unsigned long long actual = Converter::ReadVarint(input, bytesread);
	EXPECT_EQ(expected, actual);
}

TEST(TestReadVarint, NineBytes) {
	std::string input("E299AFD1FF93BAF281");
	unsigned long long expected = 14151854980018827905;
	unsigned int bytesread = 0;
	unsigned long long actual = Converter::ReadVarint(input, bytesread);
	EXPECT_EQ(expected, actual);
}

TEST(TestWriteVarint, OneByte) {
	std::string expected("20");
	unsigned long long input = 32;
	std::string actual = Converter::WriteVarint(input);
	EXPECT_STREQ(expected.c_str(), actual.c_str());
}

TEST(TestWriteVarint, TwoBytes) {
	std::string expected("8101");
	unsigned long long input = 129;
	std::string actual = Converter::WriteVarint(input);
	EXPECT_STREQ(expected.c_str(), actual.c_str());
}

TEST(TestFindingFreeblocks, FindTwoFreeblocks) {
	std::string filename("testdb.db");
	Recoverer r;
	r.Openfile(filename);
	EXPECT_EQ(4, r.GetFreeBlocks().size());
}

TEST(TestCreatingBitmasks, ParseBitmask) {
	std::vector<std::string> bitmasks{ "TEXT", "DATE" };
	std::string tablename("test_table");
	unsigned int rootpage = 1;
	std::vector<std::tuple<std::string, std::vector<std::string>, unsigned int>> input{ std::make_tuple(tablename, bitmasks, rootpage) };
	std::string filename("testdb.db");
	Recoverer r;
	r.Openfile(filename);
	EXPECT_NO_THROW(r.SetAllBitmasks(input));
}

TEST(TestRewritingFiles, CreateCopy) {
	std::string filename("testdb.db");
	Recoverer r;
	r.Openfile(filename);

	std::string copy("newlycreatedtestdb.db");
	r.ReplaceFile(copy);

	std::ifstream exp("testdb.db");
	std::string genfileorig((std::istreambuf_iterator<char>(exp)),
		std::istreambuf_iterator<char>());
	std::ifstream act("newlycreatedtestdb.db");
	std::string genfilecopy((std::istreambuf_iterator<char>(act)),
		std::istreambuf_iterator<char>());
	EXPECT_STREQ(genfileorig.c_str(), genfilecopy.c_str());
}

TEST(TestParsingDeletedRecords, Test) {
	std::vector<std::string> bitmasks{ "TEXT", "DATE" };
	std::string tablename("test_table");
	unsigned int rootpage = 1;
	std::vector<std::tuple<std::string, std::vector<std::string>, unsigned int>> input{ std::make_tuple(tablename, bitmasks, rootpage) };
	std::string filename("testdb.db");
	Recoverer r;
	r.Openfile(filename);
	r.SetAllBitmasks(input);
	auto result = r.ParseDeletedRecords();
	auto vals = result.begin();
	auto vec = vals->second;
	EXPECT_TRUE(vec.size() == 3);
}

TEST(TestRestoreSimpleRecord, TestRecoverWholeFreeblock) {
	std::vector<std::string> bitmasks{ "TEXT", "DATE" };
	std::string tablename("test_table");
	unsigned int rootpage = 1;
	std::vector<std::tuple<std::string, std::vector<std::string>, unsigned int>> input{ std::make_tuple(tablename, bitmasks, rootpage) };
	std::string filename("testdb.db");
	Recoverer r;
	r.Openfile(filename);
	r.SetAllBitmasks(input);
	auto result = r.ParseDeletedRecords();
	auto vals = result.begin();
	auto vec = vals->second;
	r.RestoreRecord(vec, 0);
	r.ReplaceFile("dbrestoredvalue.db");
	EXPECT_TRUE(1);
}

TEST(TestRestoreSimpleRecord, TestRecoverEndOfFreeblock) {
	std::vector<std::string> bitmasks{ "TEXT", "DATE" };
	std::string tablename("test_table");
	unsigned int rootpage = 1;
	std::vector<std::tuple<std::string, std::vector<std::string>, unsigned int>> input{ std::make_tuple(tablename, bitmasks, rootpage) };
	std::string filename("dbthreedeletedrecords.db");
	Recoverer r;
	r.Openfile(filename);
	r.SetAllBitmasks(input);
	auto result = r.ParseDeletedRecords();
	auto vals = result.begin();
	auto vec = vals->second;
	r.RestoreRecord(vec, 0);
	r.ReplaceFile("dbfirstrestoredvalue.db");

	std::ifstream exp("dbfirstrestoredvalue.db");
	std::string genfileorig((std::istreambuf_iterator<char>(exp)),
		std::istreambuf_iterator<char>());
	std::ifstream act("EXPdbfirstrestoredvalue.db");
	std::string genfilecopy((std::istreambuf_iterator<char>(act)),
		std::istreambuf_iterator<char>());

	EXPECT_STREQ(genfileorig.c_str(), genfilecopy.c_str());
}

TEST(TestRestoreSimpleRecord, TestRecoverMiddleOfFreeblock) {
	std::vector<std::string> bitmasks{ "TEXT", "DATE" };
	std::string tablename("test_table");
	unsigned int rootpage = 1;
	std::vector<std::tuple<std::string, std::vector<std::string>, unsigned int>> input{ std::make_tuple(tablename, bitmasks, rootpage) };
	std::string filename("dbthreedeletedrecords.db");
	Recoverer r;
	r.Openfile(filename);
	r.SetAllBitmasks(input);
	auto result = r.ParseDeletedRecords();
	auto vals = result.begin();
	auto vec = vals->second;
	r.RestoreRecord(vec, 1);
	r.ReplaceFile("dbsecrestoredvalue.db");
	std::ifstream exp("dbsecrestoredvalue.db");
	std::string genfileorig((std::istreambuf_iterator<char>(exp)),
		std::istreambuf_iterator<char>());
	std::ifstream act("EXPdbsecrestoredvalue.db");
	std::string genfilecopy((std::istreambuf_iterator<char>(act)),
		std::istreambuf_iterator<char>());

	EXPECT_STREQ(genfileorig.c_str(), genfilecopy.c_str());
}

TEST(TestRestoreSimpleRecord, TestRecoverBeginningOfFreeblock) {
	std::vector<std::string> bitmasks{ "TEXT", "DATE" };
	std::string tablename("test_table");
	unsigned int rootpage = 1;
	std::vector<std::tuple<std::string, std::vector<std::string>, unsigned int>> input{ std::make_tuple(tablename, bitmasks, rootpage) };
	std::string filename("dbthreedeletedrecords.db");
	Recoverer r;
	r.Openfile(filename);
	r.SetAllBitmasks(input);
	auto result = r.ParseDeletedRecords();
	auto vals = result.begin();
	auto vec = vals->second;
	r.RestoreRecord(vec, 2);
	r.ReplaceFile("dbthirdrestoredvalue.db");

	std::ifstream exp("dbthirdrestoredvalue.db");
	std::string genfileorig((std::istreambuf_iterator<char>(exp)),
		std::istreambuf_iterator<char>());
	std::ifstream act("EXPdbthirdrestoredvalue.db");
	std::string genfilecopy((std::istreambuf_iterator<char>(act)),
		std::istreambuf_iterator<char>());

	EXPECT_STREQ(genfileorig.c_str(), genfilecopy.c_str());
}