#ifndef VIEW_H
#define VIEW_H

#include <QWidget>
#include"DBmanager.h"
#include"GUI/Model.h"
#include<QFileDialog>
#include<QDir>
#include<QSqlDatabase>
#include<QSqlTableModel>
#include<QDebug>
#include<vector>
#include <map>
#include <tuple>
#include<GUI/HexView/QHexView.h>

#include"SQLiteLib/Recoverer.h"



namespace Ui {
class View;
}

class View : public QWidget
{
    Q_OBJECT

public:
    explicit View(QWidget *parent = nullptr);
    ~View();
    QString getPathDB();
    std::vector<std::tuple<std::string,std::vector<std::string>, unsigned int>> getTablesType();

private slots:
    void on_Open_clicked();
    void on_Delete_clicked();
    void changeTable(int);
    void changeHexTable(int);

    void on_Recover_clicked();

private:
    Ui::View *ui;
    std::shared_ptr<DBmanager> manager_;
    std::shared_ptr<Model> model_;
    std::shared_ptr<Model> deleteModel_;
    int indexHexModel_;
    int numberOfModel_;
    QString path_;
    bool openBD_;
    int size;
    Recoverer recover_;
    QHexView *vievHexWidget;
    std::vector<std::tuple<std::string,std::vector<std::string>, unsigned int>> typeTables_;
    std::map<std::string, std::vector<std::tuple<std::vector<std::string>, std::string, unsigned int, unsigned int, unsigned int, std::vector<unsigned long long>>>> data;
    std::vector <std::pair<unsigned int, std::string>> hexBlocks_;
    void updateHexTable();
    void makeRecoverDB();

};

#endif // VIEW_H
