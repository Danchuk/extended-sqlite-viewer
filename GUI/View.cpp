#include "GUI/View.h"
#include "ui_View.h"
#include<QMainWindow>
#include<QSqlQuery>
#include<QSqlError>

View::View(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::View)
{
    ui->setupUi(this);
    connect(ui->choseTable,SIGNAL(currentIndexChanged(int)),this,SLOT(changeTable(int)));
    connect(ui->chosePage,SIGNAL(currentIndexChanged(int)),this,SLOT(changeHexTable(int)));
    openBD_ = false;
    vievHexWidget = new QHexView();
    ui->gridLayout->addWidget(vievHexWidget);

}

View::~View()
{
    delete ui;
}

void View::updateHexTable()
{
    manager_.reset();
    model_.reset();
    deleteModel_.reset();
    recover_.clear();
    recover_.Openfile(path_.toStdString());
    ui->chosePage->clear();
    ui->hexView->clearSelection();
    QFile file ("deleteData");
    auto t =   file.remove();
    manager_ = std::make_shared<DBmanager>(path_);
    QSqlDatabase db = manager_->getDB();
    model_ = std::make_shared<Model>(db);
    ui->tableView->setModel(model_->getTables()[ui->choseTable->currentIndex()].get());
    typeTables_ =manager_->getTablesType();
    recover_.SetAllBitmasks(typeTables_);
    manager_->closeDB();
    vievHexWidget->clear();

    makeRecoverDB();
}
void View::on_Open_clicked()
{
    if(openBD_ == false)
    {
         path_ = QFileDialog::getOpenFileName(this,
                                                tr("Open sqlite data base"),
                                                QDir::homePath(),tr("sqlite file (*.db *.sqlite3 *.sqlite)"));

        recover_.Openfile(path_.toStdString());
        hexBlocks_ =  recover_.GetFreeBlocks();

        manager_ = std::make_shared<DBmanager>(path_);
        if(path_ != "")
        {
        ui->choseTable->show();
        QSqlDatabase db = manager_->getDB();
        model_ = std::make_shared<Model>(db);
        numberOfModel_ = 0;
        ui->tableView->setModel(model_->getTables().first().get());
        ui->choseTable->addItems(model_->getNamesOfTable());
        ui->Open->setText("Close");
        size = manager_->getDB().tables().size();
        openBD_ = true;
        typeTables_ =manager_->getTablesType();
        recover_.SetAllBitmasks(typeTables_);
        manager_->closeDB();
        makeRecoverDB();
        }
    }else{
       manager_.reset();
       model_.reset();
       deleteModel_.reset();
       ui->hexView->clearSelection();
       ui->chosePage->clear();
       ui->tableView->clearSelection();
       ui->Open->setText("Open");
       ui->choseTable->clear();
       QFile file ("deleteData");
       auto t =   file.remove();
       vievHexWidget->setData(new QHexView::DataStorageArray(QByteArray()));
       vievHexWidget->clear();

       openBD_ = false;
    }
}

void View::makeRecoverDB()
{
    data =  recover_.ParseDeletedRecords();
    if(data.size() != 0)
    {

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("deleteData");

    if(!db.open())
    {
        qDebug()<<"db not opened"<<"\n";
    }

    QSqlQuery query(db);
    query.clear();

   int rowID{};

   for( auto table : data)
   {
       size_t rows = std::get<0>(table.second[0]).size();
       QString table_name =QString::fromUtf8(table.first.c_str());


       QString create_table = "CREATE TABLE "+table_name+"DeletedRecords (id INTEGER PRIMARY KEY, ";
       QString allrows ="(id, " ;
       std::vector<std::string> nameRows;
       for(size_t i{}; i<typeTables_.size(); i++)
       {
            if (std::get<0>(typeTables_[i]) == table_name.toStdString())
            {
              nameRows =  std::get<1>(typeTables_[i]);
            }
       }
       for(size_t i{}; i<rows; i++)
       {
           if(i == rows-1)
            {
               create_table+= QString::fromStdString(nameRows[i])+" VARCHAR(100)";
               allrows+=QString::fromStdString(nameRows[i])+")";
            }
           else
           {
               create_table+=QString::fromStdString(nameRows[i])+" VARCHAR(100), ";
               allrows+= QString::fromStdString(nameRows[i])+", ";
            }
        }
       create_table+=")";
       std::string t = create_table.toStdString();
       auto b =query.exec(create_table);
       qDebug() << query.lastError();
       for(auto record : table.second)
       {
           QString inst = "INSERT INTO "+table_name + "deletedRecords "+allrows+" values(";

           inst+= QString::number(rowID++)+", ";

           for(size_t j{}; j<rows; j++)
           {
               if(j == rows-1)
                    inst+= "'"+QString::fromUtf8(std::get<0>(record)[j].c_str())+"'";
               else
               inst+="'"+QString::fromUtf8(std::get<0>(record)[j].c_str())+"', ";
           }
           inst+=")";
           bool u = query.exec(inst);
           if(!u)
           {
               qDebug()<<query.lastError()<< " "<< inst<<"\n";
           }

       }    
   }

   deleteModel_ = std::make_shared<Model>(db);
   db.close();
   indexHexModel_ = 0;

   ui->hexView->setModel(deleteModel_->getTables().first().get());
   ui->hexView->hideColumn(0);
   ui->chosePage->addItems(deleteModel_->getNamesOfTable());
   std::string strBlocks;
   auto arrFreeBlocks = recover_.GetFreeBlocks();
   for(size_t i{}; i<arrFreeBlocks.size(); i++)
       strBlocks+= arrFreeBlocks[i].second;

    QByteArray HexArrBlocks = QByteArray::fromStdString(strBlocks);
    QByteArray byteArrBlocks = QByteArray::fromHex(HexArrBlocks);

   vievHexWidget->setData(new QHexView::DataStorageArray(byteArrBlocks));


    }
}

void View::changeHexTable(int index)
{
    if(deleteModel_)
    {
       indexHexModel_ = index;
       ui->hexView->setModel(deleteModel_->getTables()[index].get());
    }
}
void View::changeTable(int index)
{
    if(model_)
    {
        numberOfModel_ = index;
        ui->tableView->setModel(model_->getTables()[index].get());
    }
}

QString View::getPathDB()
{
    return path_;
}

std::vector<std::tuple<std::string,std::vector<std::string>, unsigned int>> View::getTablesType()
{
    return manager_->getTablesType();
}

void View::on_Delete_clicked()
{

    if(openBD_ != false)
    {
        int columns = model_->getTables()[numberOfModel_].get()->columnCount();
        QString table = model_->getNamesOfTable()[ui->choseTable->currentIndex()];
        int row = ui->tableView->currentIndex().row();
        if (row != -1)
        {

        QStringList data;
        QStringList heders;
        for(int i{}; i<columns; i++)
        {
            QModelIndex index = ui->tableView->model()->index(row,i);
            data.push_back( ui->tableView->model()->data(index).toString());
            heders.push_back(ui->tableView->model()->headerData(i, Qt::Horizontal).toString());
        }
        manager_->openDB();
        manager_->deleteRecord(table,data,heders);
        qDebug()<<ui->choseTable->currentIndex();
        model_->getTables()[ui->choseTable->currentIndex()]->select();
        }
            updateHexTable();
        }
}

void View::on_Recover_clicked()
{
    if (ui->chosePage->currentIndex() != -1)
    {
    QString key =ui->chosePage->itemText(ui->chosePage->currentIndex());
    key.remove("DeletedRecords");
    auto i = ui->hexView->currentIndex().row();
    recover_.RestoreRecord(data[key.toStdString()], ui->hexView->currentIndex().row());
    recover_.ReplaceFile(path_.toStdString());
    updateHexTable();
    }
}
