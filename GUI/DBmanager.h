#ifndef DBMANAGER_H
#define DBMANAGER_H
#include<QString>
#include<QSqlDatabase>
#include<QSqlTableModel>
#include <tuple>
class DBmanager
{
public:
    DBmanager(const QString& path);
    ~DBmanager();
    QSqlDatabase& getDB();
    void deleteRecord(QString, QStringList, QStringList);
    std::vector<std::tuple<std::string,std::vector<std::string>, unsigned int>> getTablesType();
    void closeDB();
    void openDB()
    {
        db_ = QSqlDatabase::addDatabase("QSQLITE");
           db_.setDatabaseName(path_);
            db_.open();
    }

private:
    QSqlDatabase db_;
    QString path_;

};

#endif // DBMANAGER_H
