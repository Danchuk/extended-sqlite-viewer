#ifndef MODEL_H
#define MODEL_H
#include<QSqlTableModel>
#include<QList>
#include<memory>
#include<QStringList>
class Model
{
private:
    QList<std::shared_ptr<QSqlTableModel>> tables_;
    QStringList namesOfTable_;
public:
    Model(const QSqlDatabase& db);
    QList<std::shared_ptr<QSqlTableModel>> getTables();
    QStringList getNamesOfTable();
};

#endif // MODEL_H
