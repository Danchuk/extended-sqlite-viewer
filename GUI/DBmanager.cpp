#include "GUI/DBmanager.h"
#include<QDebug>
#include<QSqlQuery>
#include<QSqlError>
#include<vector>
#include <functional>


DBmanager::DBmanager(const QString& path)
{
    path_ = path;
    db_ = QSqlDatabase::addDatabase("QSQLITE");
       db_.setDatabaseName(path);
        db_.open();
       if (!db_.open())
       {
          qDebug() << "Error: connection with database fail";
       }
       else
       {
          qDebug() << "Database: connection ok";
       }
}
void DBmanager::closeDB()
{
    db_.close();
}
DBmanager::~DBmanager()
{
    if(db_.open())
           db_.close();
}

std::vector<std::tuple<std::string,std::vector<std::string>, unsigned int>> DBmanager::getTablesType()
{
    unsigned int rootpage;
    std::vector<std::tuple<std::string,std::vector<std::string>, unsigned int>> hederData;
    QSqlQuery query(db_);

    for(auto i : db_.tables())
    {
        query.exec("PRAGMA table_info("+i+")");
        std::vector<std::string> heders;
        while (query.next()) {

           if(query.value(5).toString() == "0")
           {
                heders.push_back(query.value(2).toString().toStdString());
           }
        }

       query.exec("SELECT rootpage FROM sqlite_master WHERE tbl_name = '" +i+"'");
        std::string tablename = i.toStdString();
        if (query.next())
        {

            if(query.value(0).toUInt() != 0)
                rootpage = query.value(0).toUInt();
            else
                continue;
        }
        std::tuple<std::string,std::vector<std::string>, unsigned int> temp(i.toStdString(),heders, rootpage);
        hederData.push_back(temp);
    }
    return hederData;
}
void DBmanager::deleteRecord(QString table, QStringList Data, QStringList Heders)
{
    QSqlQuery query(db_);
    if(db_.isOpen())
    {
    QString str_query = "DELETE FROM "+table+" WHERE "+Heders[0]+" = "+Data[0];
    query.prepare(str_query);
    auto success = query.exec();
    if(!success)
    {
       qDebug() << "remove error: "<< query.lastError();
    }
    }
    qDebug()<<db_.isOpen();
}


QSqlDatabase& DBmanager ::getDB()
{
    return db_;
}



