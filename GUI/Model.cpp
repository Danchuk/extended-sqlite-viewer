#include "GUI/Model.h"

Model::Model(const QSqlDatabase& db)
{
  namesOfTable_ = db.tables();
  for(int i{}; i<namesOfTable_.size(); i++)
  {
      tables_.push_back(std::make_shared<QSqlTableModel>(nullptr, db));
      tables_[i]->setTable(namesOfTable_[i]);
      tables_[i]->select();
  }
}

QList<std::shared_ptr<QSqlTableModel>> Model::getTables()
{
    return tables_;
}
QStringList Model::getNamesOfTable()
{
    return namesOfTable_;
}
