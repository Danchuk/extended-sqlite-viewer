
#include "Recoverer.h"
#include<limits.h>


Recoverer::Recoverer()
{
}

void Recoverer::clear()
{
    m_hexChars.clear();
    m_freeblocks.clear();
    m_foundedValues.clear();
    m_bitmask.clear();
}

std::map<std::string, int> Recoverer::m_affinitiesmap{
{"INT" , 1}, {"INTEGER" , 1},
{"TINYINT" , 1}, {"SMALLINT" , 1},
{"MEDIUMINT" , 1}, {"BIGINT" , 1},
{"UNSIGNED BIG INT" , 1}, {"INT2" , 1},
{"INT8" , 1}, {"FLOATING POINT", 1},
{"CHARACTER" , 2},
{"VARCHAR" , 2}, {"VARYING CHARACTER" , 2},
{"NCHAR" , 2}, {"NATIVE CHARACTER" , 2},
{"NVARCHAR" , 2}, {"TEXT" , 2},
{"CLOB" , 2}, {"BLOB" , 3},
{"" , 3}, {"REAL" , 4},
{"DOUBLE" , 4}, {"DOUBLE PRECISION" , 4},
{"FLOAT" , 4},{"NUMERIC" , 5},
{"DECIMAL" , 5}, {"BOOLEAN" , 5},
{"DATE" , 5}, {"DATETIME" , 5},
{"STRING", 5}
};

void Recoverer::RestoreRecord(std::vector<std::tuple<std::vector<std::string>,std::string, unsigned int, unsigned int, unsigned int, std::vector<unsigned long long>>>& records
                                    , const unsigned int& index)
{
    if (index < records.size() && index != 0)
    {
        if (std::get<1>(records[index]) == std::get<1>(records[index - 1]) ||
			std::get<1>(records[index]) == std::get<1>(records[index + 1]))
        {

			std::pair<unsigned int, std::string> rec = std::make_pair(std::get<2>(records[index]), std::get<1>(records[index]));
			auto headers = std::get<5>(records[index]);
			std::pair<unsigned int, unsigned int> range = std::make_pair(std::get<3>(records[index]), std::get<4>(records[index]));
			RestorePartOfRecord(rec, index, headers, range);
        }
        else
        {
			std::pair<unsigned int, std::string> rec = std::make_pair(std::get<2>(records[index]), std::get<1>(records[index]));
			auto headers = std::get<5>(records[index]);
			RestoreSimpleRecord(rec, index, headers);
        }
    }
    else if (index == 0)
    {
        if (records.size() != 1)
        {
            if (std::get<1>(records[0]) == std::get<1>(records[1]))
            {
				std::pair<unsigned int, std::string> rec = std::make_pair(std::get<2>(records[index]), std::get<1>(records[index]));
				auto headers = std::get<5>(records[index]);
				std::pair<unsigned int, unsigned int> range = std::make_pair(std::get<3>(records[index]), std::get<4>(records[index]));
				RestorePartOfRecord(rec, index, headers, range);
            }
            else
            {
				std::pair<unsigned int, std::string> rec = std::make_pair(std::get<2>(records[index]), std::get<1>(records[index]));
				auto headers = std::get<5>(records[index]);
				RestoreSimpleRecord(rec, index, headers);
            }
        }
        else
        {
			std::pair<unsigned int, std::string> rec = std::make_pair(std::get<2>(records[index]), std::get<1>(records[index]));
			auto headers = std::get<5>(records[index]);
			RestoreSimpleRecord(rec, index, headers);
        }
    }
    else
    {
        if (std::get<1>(records[index]) == std::get<1>(records[index - 1]))
        {
			std::pair<unsigned int, std::string> rec = std::make_pair(std::get<2>(records[index]), std::get<1>(records[index]));
			auto headers = std::get<5>(records[index]);
			std::pair<unsigned int, unsigned int> range = std::make_pair(std::get<3>(records[index]), std::get<4>(records[index]));
			RestorePartOfRecord(rec, index, headers, range);
        }
        else
        {
			std::pair<unsigned int, std::string> rec = std::make_pair(std::get<2>(records[index]), std::get<1>(records[index]));
			auto headers = std::get<5>(records[index]);
			RestoreSimpleRecord(rec, index, headers);
        }
    }
}

void Recoverer::Openfile(const std::string& filename)
{
    std::vector <unsigned char> m_binaryChars;
    std::ifstream file(filename, std::ifstream::binary);
    if (file.is_open())
    {
        file.unsetf(std::ios::skipws);

        file.seekg(0, std::ios::end);
        std::streampos m_file = file.tellg();
        file.seekg(0, std::ios::beg);

        m_binaryChars.reserve(m_file);

        std::copy(std::istream_iterator<unsigned char>(file), std::istream_iterator<unsigned char>(), std::back_inserter(m_binaryChars));
        m_hexChars.clear();
        m_bitmask.clear();
        m_freeblocks.clear();
        Converter::RawToHex(m_binaryChars, m_hexChars);
        m_freeblocks = m_gatherer.LoopThroughPages(m_file, m_hexChars, m_pageSize);
        file.close();
    }
}

void Recoverer::SetAllBitmasks(std::vector<std::tuple<std::string, std::vector<std::string>, unsigned int>>& tableinfo)
{
    m_bitmask.clear();
    for (auto table : tableinfo)
    {
        if (std::get<0>(table) == "sqlite_sequence")
        {
            continue;
        }
        std::vector<std::string> bitmask;
        for (auto types : std::get<1>(table))
        {
            bitmask.push_back(Affinity(types));
        }
        auto root = std::get<2>(table) - 1;
        std::vector <unsigned int> pages =  GatherAllLeaves(root);
        m_bitmask.push_back(std::make_tuple(std::get<0>(table), bitmask, pages));
    }
}

std::map<std::string, std::vector<std::tuple<std::vector<std::string>, std::string, unsigned int, unsigned int, unsigned int, std::vector<unsigned long long>>>> Recoverer::ParseDeletedRecords()
{
    std::map<std::string, std::vector<std::tuple<std::vector<std::string>, std::string, unsigned int, unsigned int, unsigned int, std::vector<unsigned long long>>>> m_map;
    for (auto freeblock : m_freeblocks)
    {
        for (auto bitmask : m_bitmask)
        {
            if (std::find(std::get<2>(bitmask).begin(), std::get<2>(bitmask).end(), freeblock.first)
                    != std::get<2>(bitmask).end())
            {
                FindDeletedRecord(freeblock.second, std::get<1>(bitmask));
                while(!m_foundedValues.empty())
                {
                    std::tuple<std::vector<std::string>, unsigned int, unsigned int, std::vector<unsigned long long>> restored = m_foundedValues.back();
                    m_foundedValues.pop_back();
                    m_map[std::get<0>(bitmask)].push_back(std::make_tuple(std::get<0>(restored), freeblock.second, freeblock.first, std::get<1>(restored), std::get<2>(restored), std::get<3>(restored)));
                }
            }
        }
    }
    return m_map;
}


std::vector<std::string> Recoverer::GatherDeletedRecord(std::vector<unsigned long long>& recordHeader, std::string &freeblock, unsigned int& offset)
{
    std::vector<std::string> restoredValue;
    for(auto type : recordHeader)
    {
        if (type >= 1 && type <= 6)
        {
            long long value = RestoreInteger(freeblock, type, offset);
            restoredValue.push_back(std::to_string(value));
        }
        else if (type == 7)
        {
            if (freeblock.length() < 16)
            {
                throw std::exception();
            }
            std::string value = std::string(freeblock.begin(), freeblock.begin() + 16);
            freeblock = std::string(freeblock.begin() + 16, freeblock.end());
            float ValueRestored = *((float*)Converter::ConvertFromHex(value));
            restoredValue.push_back(std::to_string(ValueRestored));
            freeblock = std::string(freeblock.begin() + 16, freeblock.end());
            offset += 16;
        }
        else if (type >= 12 && type % 2 == 0)
        {
            if (freeblock.length() < 2 * ((type - 12) / 2))
            {
                throw  std::exception();
            }
            std::string value = std::string(freeblock.begin(), freeblock.begin() +  2 * ((type - 12) / 2));
            std::string text = Converter::ConvertToText(value);
            restoredValue.push_back(text);
            freeblock = std::string(freeblock.begin() + 2 * ((type - 12) / 2), freeblock.end());
            offset += 2 * ((type - 12) / 2);
        }
        else
        {
            if (freeblock.length() < 2 * ((type - 13) / 2))
            {
                throw  std::exception();
            }
            std::string value = std::string(freeblock.begin(), freeblock.begin() + 2 * ((type - 13) / 2));
            std::string text = Converter::ConvertToText(value);
            restoredValue.push_back(text);
            freeblock = std::string(freeblock.begin() + 2 * ((type - 13) / 2), freeblock.end());
            offset += 2 * ((type - 13) / 2);
        }
    }
    return restoredValue;
}

void Recoverer::ReplaceFile(const std::string &filename)
{
    std::vector<unsigned char> binarydata;
    binarydata.reserve(m_hexChars.length() / 2);
    for (std::size_t i = 0; i < m_hexChars.length(); i += 2)
    {
        std::string byte = m_hexChars.substr(i, 2);
        unsigned char chr = (unsigned char)(int)strtol(byte.c_str(), NULL, 16);
        binarydata.push_back(chr);
    }
    std::ofstream fs(filename, std::ofstream::out | std::ios::binary);
    if (fs.is_open())
    {
        fs.write((char*)&binarydata[0], binarydata.size() * sizeof(unsigned char));
        fs.close();
    }
}

Recoverer::~Recoverer()
{
}

std::string Recoverer::Affinity(const std::string & type)
{
    switch (m_affinitiesmap[type])
    {
    case 1:
        return "INTEGER";
    case 2:
        return "TEXT";
    case 3:
        return "BLOB";
    case 4:
        return "REAL";
    case 5:
        return "NUMERIC";
    }
}

void Recoverer::TraverseBTreeRec(std::vector<unsigned int>& leaves, unsigned int page)
{
    std::string header(m_hexChars.begin() + 2 * (page * m_pageSize),
                       m_hexChars.begin() + 2 * (page * m_pageSize + 1));
    if (header == "0D")
    {
        leaves.push_back(page);
    }
    else if (header == "05")
    {
        std::string amountofchildren(m_hexChars.begin() + 2 * (page * m_pageSize) + 6,
                                     m_hexChars.begin() + 2 * (page * m_pageSize) + 10);
        unsigned int amount = Converter::ConvertFromHex(amountofchildren);
        for (std::size_t i = 0; i < amount; i++)
        {
            std::string childoffset(m_hexChars.begin() + 2 * (page * m_pageSize + 12) + 4 * i,
                                    m_hexChars.begin() + 2 * (page * m_pageSize + 12) + 4 * (i + 1));
            unsigned int ichildoffset = Converter::ConvertFromHex(childoffset);
            std::string pagenum(m_hexChars.begin() + 2 * (page * m_pageSize + ichildoffset), m_hexChars.begin() + 2 * (page * m_pageSize + ichildoffset + 4));
            unsigned int page = Converter::ConvertFromHex(pagenum);
            TraverseBTreeRec(leaves, page);
        }
    }
}

void Recoverer::FindDeletedRecord(std::string freeblock, std::vector<std::string>& bitmask)
{
    std::vector<unsigned long long> headerValues;
    unsigned int offset = 0;
    unsigned int initOffset = 0;
    while (freeblock.length() != 0)
    {
        std::string possibleRecord;
        if (freeblock.length() >= 8)
        {
            possibleRecord = std::string(freeblock.begin() + 8, freeblock.end());
            initOffset += 8;
        }
        else
        {
            possibleRecord = std::string(freeblock.begin(), freeblock.end());
            initOffset += possibleRecord.length();
        }
        for (std::size_t i = 0; i < bitmask.size(); i++)
        {
            unsigned int fieldsize = 0;
            std::string possibleValue;
            if (possibleRecord.length() < 18)
            {
                if (possibleRecord.length() == 0)
                {
                    break;
                }
                possibleValue = std::string(possibleRecord.begin(), possibleRecord.end());
            }
            else
            {
                possibleValue = std::string(possibleRecord.begin(), possibleRecord.begin() + 18);
            }
            unsigned long long value = Converter::ReadVarint(possibleValue, fieldsize);
            if (ValueCorrespondsMask(value, bitmask[i]))
            {
                headerValues.push_back(value);
            }
            else
            {
                headerValues.clear();
                i = -1;
            }
            initOffset += 2 * fieldsize;
            possibleRecord = std::string(possibleRecord.begin() + 2 * fieldsize, possibleRecord.end());
        }
        try {
            if (headerValues.size() != 0)
            {
                std::vector<std::string> values = GatherDeletedRecord(headerValues, possibleRecord, initOffset);
                m_foundedValues.push_back(std::make_tuple(values, offset, initOffset, headerValues));
            }
            offset = initOffset;
            freeblock = std::string(possibleRecord.begin(), possibleRecord.end());
        } catch (const std::exception&) {
            freeblock = std::string(possibleRecord.begin(), possibleRecord.end());
            continue;
        }
        headerValues.clear();
    }
}

bool Recoverer::ValueCorrespondsMask(unsigned long long & value, std::string & type)
{
    if ((value >= 1 && value <= 6) && (type == "INTEGER" || type == "NUMERIC"))
    {
        return true;
    }
    if (value == 7 && (type == "REAL" || type == "NUMERIC"))
    {
        return true;
    }
    if (value >= 12 && value % 2 == 0 && type == "BLOB")
    {
        return true;
    }
    if (value >= 13 && value % 2 != 0 && (type == "TEXT" || type == "NUMERIC"))
    {
        return true;
    }
    return false;
}

long long Recoverer::RestoreInteger(std::string &freeblock, unsigned long long &type, unsigned int& offset)
{
    switch (type) {
    case 1:
    {
        if (freeblock.length() < 2)
        {
            throw std::exception();
        }
        std::string value(freeblock.begin(), freeblock.begin() + 2);
        long long restored = Converter::ConvertFromHex(value);
        freeblock = std::string(freeblock.begin() + 2, freeblock.end());
        offset += 2;
        return restored;
    }
    case 2:
    {
        if (freeblock.length() < 4)
        {
            throw std::exception();
        }
        std::string value(freeblock.begin(), freeblock.begin() + 4);
        long long restored = Converter::ConvertFromHex(value);
        freeblock = std::string(freeblock.begin() + 4, freeblock.end());
        offset += 4;
        return restored;
    }
    case 3:
    {
        if (freeblock.length() < 6)
        {
            throw std::exception();
        }
        std::string value(freeblock.begin(), freeblock.begin() + 6);
        long long restored = Converter::ConvertFromHex(value);
        freeblock = std::string(freeblock.begin() + 6, freeblock.end());
        offset += 6;
        return restored;
    }
    case 4:
    {
        if (freeblock.length() < 8)
        {
            throw std::exception();
        }
        std::string value(freeblock.begin(), freeblock.begin() + 8);
        long long restored = Converter::ConvertFromHex(value);
        freeblock = std::string(freeblock.begin() + 8, freeblock.end());
        offset += 8;
        return restored;
    }
    case 5:
    {
        if (freeblock.length() < 12)
        {
            throw std::exception();
        }
        std::string value(freeblock.begin(), freeblock.begin() + 12);
        long long restored = Converter::ConvertFromHex(value);
        freeblock = std::string(freeblock.begin() + 12, freeblock.end());
        offset += 12;
        return restored;
    }
    case 6:
    {
        if (freeblock.length() < 16)
        {
            throw std::exception();
        }
        std::string value(freeblock.begin(), freeblock.begin() + 16);
        long long restored = Converter::ConvertFromHex(value);
        freeblock = std::string(freeblock.begin() + 16, freeblock.end());
        offset += 16;
        return restored;
    }
    default:
        return 0;
    }
}

std::vector<unsigned int> Recoverer::GatherAllLeaves(unsigned int& root)
{
    std::vector<unsigned int> pages;
    std::string header(m_hexChars.begin() + 2 * (root * m_pageSize), m_hexChars.begin() + 2 * (root * m_pageSize + 1));
    if (header != "02" || header != "0A")
    {
        TraverseBTreeRec(pages, root);
    }
    return pages;
}

void Recoverer::RedirectFreeblockNode(unsigned int &newoffset, std::pair<unsigned int, std::string>&freeblock)
{
	unsigned int pageoffset = freeblock.first * m_pageSize;
	std::string pointertofreeblock(m_hexChars.begin() + 2 * pageoffset + 2, m_hexChars.begin() + 2 * pageoffset + 6);
	auto found = std::find(m_freeblocks.begin(), m_freeblocks.end(), freeblock);
	unsigned int index = found - m_freeblocks.begin();
	std::stringstream stringstr;
	stringstr.clear();
	stringstr << std::setw(4) << std::setfill('0') << std::hex << std::uppercase << newoffset;
	if (index != 0 && m_freeblocks[index - 1].first == freeblock.first)
	{
		std::string locationofprev;
		unsigned int prevofset;
		if (index >= 2 && m_freeblocks[index - 2].first == freeblock.first)
		{
			locationofprev = std::string(m_freeblocks[index - 2].second.begin(), m_freeblocks[index - 2].second.begin());
			prevofset = Converter::ConvertFromHex(locationofprev);
		}
		else
		{
			locationofprev = std::string(m_hexChars.begin() + 2 * (freeblock.first * m_pageSize) + 2,
				m_hexChars.begin() + 2 * (freeblock.first * m_pageSize) + 6);
			prevofset = Converter::ConvertFromHex(locationofprev);
		}
		m_hexChars.replace(m_hexChars.begin() + 2 * (freeblock.first * m_pageSize + prevofset),
			m_hexChars.begin() + 2 * (freeblock.first * m_pageSize + prevofset) + 4, stringstr.str());
	}
	else
	{
		m_hexChars.replace(m_hexChars.begin() + 2 * (freeblock.first * m_pageSize) + 2, m_hexChars.begin() + 2 * (freeblock.first * m_pageSize) + 6, stringstr.str());
	}
}

unsigned int Recoverer::RemoveWholeFreeblock(std::pair<unsigned int, std::string>& freeblock)
{
    unsigned int offset = 0;
    unsigned int pageoffset = freeblock.first * m_pageSize;
    std::string nextfreeblock(freeblock.second.begin(), freeblock.second.begin() + 4);
    std::string pointertofreeblock(m_hexChars.begin() + 2 * pageoffset + 2, m_hexChars.begin() + 2 * pageoffset + 6);
    auto found = std::find(m_freeblocks.begin(), m_freeblocks.end(), freeblock);
    unsigned int index = found - m_freeblocks.begin();
    if (index != 0 && m_freeblocks[index - 1].first == m_freeblocks[index].first)
    {
        std::string pointertoprev;
        if (index >= 2 && m_freeblocks[index - 2].first == m_freeblocks[index].first)
        {
            pointertoprev = std::string(m_freeblocks[index - 2].second.begin(),
                                        m_freeblocks[index - 2].second.begin() + 4);
        }
        else
        {
            pointertoprev = std::string(m_hexChars.begin() + 2 * pageoffset + 2,
                                        m_hexChars.begin() + 2 * pageoffset + 6);
        }
        unsigned int offsettoprev = Converter::ConvertFromHex(pointertoprev);
        std::string prevfreeblock(m_freeblocks[index - 1].second.begin(),
                                  m_freeblocks[index - 1].second.begin() + 4);
        offset = Converter::ConvertFromHex(prevfreeblock);

        m_hexChars.replace(m_hexChars.begin() + 2 * pageoffset + 2 * offsettoprev,
                           m_hexChars.begin() + 2 * pageoffset + 2 * offsettoprev + 4, nextfreeblock);
    }
    else
    {
        m_hexChars.replace(m_hexChars.begin() + 2 * pageoffset + 2,
                           m_hexChars.begin() + 2 * pageoffset + 6, nextfreeblock);
        offset = Converter::ConvertFromHex(pointertofreeblock);
    }
    return offset;
}

unsigned int Recoverer::GetOffsetToFreeblock(std::pair<unsigned int, std::string>& freeblock)
{
	unsigned int offset = 0;
	unsigned int pageoffset = freeblock.first * m_pageSize;
	std::string pointertofreeblock(m_hexChars.begin() + 2 * pageoffset + 2, m_hexChars.begin() + 2 * pageoffset + 6);
	auto found = std::find(m_freeblocks.begin(), m_freeblocks.end(), freeblock);
	unsigned int index = found - m_freeblocks.begin();
	if (index != 0 && m_freeblocks[index - 1].first == freeblock.first)
	{
		std::string pointertonextfreeblock(m_freeblocks[index - 1].second.begin(),
										m_freeblocks[index - 1].second.begin() + 4);
		offset = Converter::ConvertFromHex(pointertonextfreeblock);
	}
	else
	{
		offset = Converter::ConvertFromHex(pointertofreeblock);
	}
	return offset;
}

unsigned long long Recoverer::GetRowid(unsigned int& pagenum, unsigned int& offset)
{
    std::string possibleHeaderSize(m_hexChars.begin() + 2 * ((pagenum * m_pageSize) + offset),
                                   m_hexChars.begin() + 2 * ((pagenum * m_pageSize) + offset) + 18);
    unsigned int bytes = 0;
    Converter::ReadVarint(possibleHeaderSize, bytes);
    std::string possibleRowId(m_hexChars.begin() + 2 * ((pagenum * m_pageSize) + offset + bytes),
                              m_hexChars.begin() + 2 * ((pagenum * m_pageSize) + offset + bytes) + 18);
    bytes = 0;
    unsigned long long rowid = Converter::ReadVarint(possibleRowId, bytes);
    return rowid;
}

unsigned long long Recoverer::UpdatePointerArray(unsigned int& pagenum,const unsigned int& offset,const unsigned int& index, const unsigned int& elements)
{
    unsigned long long rowid = LONG_MAX;
    unsigned int pageOffset = pagenum * m_pageSize;
	std::string currentNums = std::string(m_hexChars.begin() + 2 * pageOffset + 8,
										m_hexChars.begin() + 2 * pageOffset + 10);
    unsigned int numOfValues = Converter::ConvertFromHex(currentNums);
    numOfValues++;
    std::stringstream stringstr;
	stringstr.clear();
	stringstr << std::setw(2) << std::setfill('0') << std::hex << std::uppercase << numOfValues;
	std::string nums(stringstr.str());
    m_hexChars.replace(m_hexChars.begin() + 2 * pageOffset + 8,
        m_hexChars.begin() + 2 * pageOffset + 10, nums);
    std::string ptrarr(m_hexChars.begin() + 2 * pageOffset + 16, m_hexChars.begin() + 2 * (pageOffset + 2 * numOfValues) + 16);
    bool newRecordIsStored = true;
	unsigned int secondpointer = offset;
    for (std::size_t i = 0; i < numOfValues; i++)
    {
       unsigned int pointer = Converter::ConvertFromHex(std::string(ptrarr.begin() + 4 * i,
           ptrarr.begin() + 4 * (i + 1)));
	   if (i == numOfValues - 1)
	   {
		   std::stringstream stringstr;
		   stringstr << std::setw(4) << std::setfill('0') << std::hex << std::uppercase << secondpointer;
		   m_hexChars.replace(m_hexChars.begin() + 2 * pageOffset + 10,
			   m_hexChars.begin() + 2 * pageOffset + 14, stringstr.str());
	   }
       if (pointer < secondpointer)
       {
           if (newRecordIsStored)
           {
               if (i == 0)
               {
                   rowid = GetRowid(pagenum, pointer);
                   rowid -= (elements - index);
               }
               newRecordIsStored = false;
           }
		   std::stringstream stringstr;
           stringstr << std::setw(4) << std::setfill('0') <<std::hex << std::uppercase << secondpointer;
           secondpointer = pointer;
		   std::string s(stringstr.str());
           ptrarr.replace(ptrarr.begin() + 4 * i, ptrarr.begin() + 4 * (i + 1), stringstr.str());
       }
       else
       {
           if (newRecordIsStored)
           {
               rowid = GetRowid(pagenum, pointer);
               rowid += index;
               rowid++;
           }
       }
    }
    m_hexChars.replace(m_hexChars.begin() + 2 * pageOffset + 16, m_hexChars.begin() + 2 * (pageOffset + 2 * numOfValues) + 16, ptrarr);
    return rowid;
}

void Recoverer::RestoreRecordHeader(unsigned int& offset, unsigned long long& rowid, unsigned int& size, std::vector<unsigned long long>& headers)
{
    std::string freeblockHeader(m_hexChars.begin() + 2 * offset, m_hexChars.begin() + 2 * offset + 18);
    std::string headersize = Converter::WriteVarint(size - 2);
    std::string srowid = Converter::WriteVarint(rowid);
	unsigned long long typelength = 1;
	for (auto val : headers)
	{
		typelength += Converter::WriteVarint(val).length() / 2;
	}
	typelength++;
	std::string typesize(Converter::WriteVarint(typelength));
	freeblockHeader.replace(freeblockHeader.begin(), freeblockHeader.begin() + headersize.length(), headersize);
    freeblockHeader.replace(freeblockHeader.begin() + headersize.length(), freeblockHeader.begin() + headersize.length() + srowid.length(), srowid);
	freeblockHeader.replace(freeblockHeader.begin() + headersize.length() + srowid.length(), freeblockHeader.begin() + headersize.length() + srowid.length() + typesize.length(), typesize);
	freeblockHeader.replace(freeblockHeader.begin() + headersize.length() + srowid.length() + typesize.length(), freeblockHeader.begin() + headersize.length() + srowid.length() + typesize.length() + 2, "00");
    m_hexChars.replace(m_hexChars.begin() + 2 * offset, m_hexChars.begin() + 2 * offset + 18, freeblockHeader);
}

void Recoverer::RestoreSimpleRecord(std::pair<unsigned int, std::string>& restoredRecord, const unsigned int& index, std::vector<unsigned long long>& headers)
{
    unsigned int offset = RemoveWholeFreeblock(restoredRecord);
    unsigned int shared_records = 0;
    for (auto f : m_freeblocks)
    {
        if (f == restoredRecord)
        {
            shared_records++;
        }
    }
    unsigned long long rowid = UpdatePointerArray(restoredRecord.first, offset, index, shared_records);
    unsigned int hSize = restoredRecord.second.length() / 2;
	offset += restoredRecord.first * m_pageSize;
    RestoreRecordHeader(offset, rowid, hSize, headers);
}

void Recoverer::RestorePartOfRecord(std::pair<unsigned int, std::string>& freeblock, const unsigned int & index, std::vector<unsigned long long>& headers, std::pair<unsigned int, unsigned int>& range)
{
	if (range.first == 0)
	{
		RestoreBeginning(freeblock, index, headers, range);
	}
	else if (range.second == freeblock.second.length())
	{
		RestoreEnding(freeblock, index, headers, range);
	}
	else
	{
		RestoreMiddle(freeblock, index, headers, range);
	}
}

void Recoverer::RestoreBeginning(std::pair<unsigned int, std::string>& freeblock, const unsigned int& index, std::vector<unsigned long long>& headers, std::pair<unsigned int, unsigned int>& range)
{
	std::stringstream stringstr;
	stringstr << std::setw(4) << std::setfill('0') << std::hex << std::uppercase << (freeblock.second.length() - (range.second - range.first)) / 2;
	std::string currentfreeblock(freeblock.second);
	unsigned int offset = GetOffsetToFreeblock(freeblock);
	unsigned int newPointer = offset + range.second / 2;
	RedirectFreeblockNode(newPointer, freeblock);

	std::string newheader(currentfreeblock.begin(), currentfreeblock.begin() + 4);
	newheader += stringstr.str();
	currentfreeblock.replace(currentfreeblock.begin() + range.second, currentfreeblock.begin() + range.second + 8, newheader);


	m_hexChars.replace(m_hexChars.begin() + 2 * (offset + m_pageSize * freeblock.first), m_hexChars.begin() + 2 * (offset + m_pageSize * freeblock.first) + freeblock.second.length(), currentfreeblock);

    unsigned int shared_records = 0;
    for (auto f : m_freeblocks)
    {
        if (f == freeblock)
        {
            shared_records++;
        }
    }
    unsigned long long rowid = UpdatePointerArray(freeblock.first, offset, index, shared_records);
	unsigned int size = (range.second - range.first) / 2;
	unsigned int recordoffset = offset + m_pageSize * freeblock.first;
	RestoreRecordHeader(recordoffset, rowid, size, headers);
}

void Recoverer::RestoreEnding(std::pair<unsigned int, std::string>& freeblock, const unsigned int &index, std::vector<unsigned long long>& headers, std::pair<unsigned int, unsigned int>& range)
{
	std::stringstream stringstr;
	stringstr << std::setw(4) << std::setfill('0') << std::hex << std::uppercase << range.first / 2;
	std::string currentfreeblock(freeblock.second);
	unsigned int offset = GetOffsetToFreeblock(freeblock);

	currentfreeblock.replace(currentfreeblock.begin() + 4, currentfreeblock.begin() + 8, stringstr.str());
	m_hexChars.replace(m_hexChars.begin() + 2 * (m_pageSize * freeblock.first + offset),
		m_hexChars.begin() + 2 * (m_pageSize * freeblock.first + offset) + currentfreeblock.length(), currentfreeblock);
	offset += range.first / 2;

    unsigned int shared_records = 0;
    for (auto f : m_freeblocks)
    {
        if (f == freeblock)
        {
            shared_records++;
        }
    }
    unsigned long long rowid = UpdatePointerArray(freeblock.first, offset, index, shared_records);
	unsigned int size = (range.second - range.first) / 2;
	offset += m_pageSize * freeblock.first;
	RestoreRecordHeader(offset, rowid, size, headers);
}

void Recoverer::RestoreMiddle(std::pair<unsigned int, std::string>& freeblock, const unsigned int &index, std::vector<unsigned long long>& headers, std::pair<unsigned int, unsigned int>& range)
{
	std::string currentfreeblock(freeblock.second);
	unsigned int offset = GetOffsetToFreeblock(freeblock);

	std::stringstream stringstr;
	stringstr << std::setw(4) << std::setfill('0') << std::hex << std::uppercase << range.first / 2;
	currentfreeblock.replace(currentfreeblock.begin() + 4, currentfreeblock.begin() + 8, stringstr.str());
	stringstr.str("");

	std::string nextfreeblock(currentfreeblock.begin(), currentfreeblock.begin() + 4);
	stringstr << std::setw(4) << std::setfill('0') << std::hex << std::uppercase << offset + range.second / 2;
	currentfreeblock.replace(currentfreeblock.begin(), currentfreeblock.begin() + 4, stringstr.str());
	stringstr.str("");

	currentfreeblock.replace(currentfreeblock.begin() + range.second, currentfreeblock.begin() + range.second + 4, nextfreeblock);
	stringstr << std::setw(4) << std::setfill('0') << std::hex << std::uppercase << (currentfreeblock.length() - range.second) / 2;
	currentfreeblock.replace(currentfreeblock.begin() + range.second + 4, currentfreeblock.begin() + range.second + 8, stringstr.str());


	m_hexChars.replace(m_hexChars.begin() + 2 * (m_pageSize * freeblock.first + offset),
		m_hexChars.begin() + 2 * (m_pageSize * freeblock.first + offset) + currentfreeblock.length(), currentfreeblock);

    offset += range.first / 2;
    unsigned int shared_records = 0;
    for (auto f : m_freeblocks)
    {
        if (f == freeblock)
        {
            shared_records++;
        }
    }
    unsigned long long rowid = UpdatePointerArray(freeblock.first, offset, index, shared_records);
	unsigned int size = (range.second - range.first) / 2;
	offset += m_pageSize * freeblock.first;
	RestoreRecordHeader(offset, rowid, size, headers);
}

std::vector<std::pair<unsigned int, std::string> > Recoverer::GetFreeBlocks()
{
    return m_freeblocks;
}
