#include "Converter.h"

const std::string Converter::m_hex = "0123456789ABCDEF";

void Converter::RawToHex(const std::vector<unsigned char>& data, std::string & str)
{
	Converter::RawToHex(&data[0], data.size(), str);
}

unsigned long long Converter::ConvertFromHex(std::string &str)
{
    return std::strtoull(str.c_str(), nullptr, 16);
}

unsigned long long Converter::ConvertFromHex(std::string &&str)
{
    return std::strtoull(str.c_str(), nullptr, 16);
}

std::string Converter::ConvertToText(std::string &str)
{
	std::string answer = "";
	for (size_t i = 0; i < str.length(); i += 2)
	{
		std::string byte = str.substr(i, 2);
		char chr = (char)(int)strtol(byte.c_str(), NULL, 16);
		answer.push_back(chr);
	}
	return answer;
}

std::string Converter::ConvertToText(std::string &&str)
{
	std::string answer = "";
	for (size_t i = 0; i < str.length(); i += 2)
	{
		std::string byte = str.substr(i, 2);
		char chr = (char)(int)strtol(byte.c_str(), NULL, 16);
		answer.push_back(chr);
	}
	return answer;
}

unsigned long long Converter::ReadVarint(const std::string& freeblock, unsigned int& bytescaptured)
{
    unsigned long long field;
	std::vector<unsigned char> extension;
	for (int i = 0; i < 18; i += 2)
	{
        unsigned char t = Converter::ConvertFromHex(std::string(freeblock.begin() + i, freeblock.begin() + i + 2));
		extension.push_back(t);
        bytescaptured++;
        if (t < 128 || 2 * bytescaptured == freeblock.length())
		{
			break;
		}
	}
	std::string bitset = "";
	GatherBits(bitset, extension);
	field = std::strtoull(bitset.c_str(), nullptr, 2);
    return field;
}

std::string Converter::WriteVarint(const unsigned long long &integer)
{
    unsigned char bytes[9];
    std::string varint;
    int i, j;
    for (i = 9; i > 0; i--) {
        if (integer & 127ULL << i * 7) break;
    }
    for (j = 0; j <= i; j++)
        bytes[j] = ((integer >> ((i - j) * 7)) & 127) | 128;

    bytes[i] ^= 128;
    RawToHex(bytes, j, varint);
    return varint;
}

void Converter::RawToHex(const unsigned char * data, size_t length, std::string & str)
{
	str.resize(length * 2, ' ');
	for (size_t i = 0; i < length; i++)
	{
		str[i << 1] = m_hex[(data[i] >> 4) & 0x0f];
		str[(i << 1) + 1] = m_hex[data[i] & 0x0f];
	}
}

void Converter::GatherBits(std::string &bitset, std::vector<unsigned char> chars)
{
	for (std::size_t i = 0; i < chars.size(); i++)
	{
		std::bitset<8> bits(chars[i]);
		if (chars.size() == 9)
		{
			if (i == 8)
			{
				bitset += bits.to_string();
			}
			else
			{
				std::string bitsstr = bits.to_string();
				bitset += std::string(bitsstr.begin() + 1, bitsstr.end());
			}
		}
		else
		{
			std::string bitsstr = bits.to_string();
			bitset += std::string(bitsstr.begin() + 1, bitsstr.end());
		}
	}
}
