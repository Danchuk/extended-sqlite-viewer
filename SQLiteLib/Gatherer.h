#pragma once
#include "Converter.h"
#include <iostream>

class Gatherer
{
public:
    void GatherFreeblocks(std::string&, unsigned int&, unsigned int&, unsigned int&, std::vector <std::pair<unsigned int, std::string>>&);
    std::vector <std::pair<unsigned int, std::string>> LoopThroughPages(std::streampos&,std::string&, unsigned int&);
    bool IsValidPage(const std::string&);
};
